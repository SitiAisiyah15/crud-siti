<!DOCTYPE html>
<html>
<head>
	<title>Tampil Pasien</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'bootstrap/css/bootstrap.css'?>">
	 <script type="text/javascript" src="<?php echo base_url('asset/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>

</head>
<body>
    <br>
    <div class="container-fluid">
	 <div class="panel panel-primary">
        <div class="panel-heading">
            <div align="right">
            <center><h3><b class="col-md-10">RIWAYAT PENYAKIT</b></h3></center>
                <button data-toggle="modal" data-target="#addModal" class="btn btn-success"><b>+ </b>Tambah Data</button>
                <a href="<?=base_url()?>index.php"><button class="btn btn-link">BACK</button></a>
            </div>
        </div>
        <br>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="bg-primary">
                            <th>No</th>
                            <th>ID</th>
							<th>Nama</th>
							<th>Usia</th>
							<th>Alamat</th>
							<th>Tanggal Periksa</th>
							<th>Keluhan</th>
                            <th>Diagnosis</th>
							<th>Dokter</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody id="tbl_data">
                         <!-- isi tabel-->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
 
    <!-- Modal Tambah-->
    <div id="addModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
 
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Tambah Data</h4>
          </div>
          <div class="modal-body">
            <form>
               
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="usia">Usia</label>
                    <input type="text" name="usia" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" name="alamat" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="tgl">Tanggal Periksa</label>
                    <input type="date" name="tgl" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="keluhan">Keluhan</label>
                    <input type="text" name="keluhan" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="diagnosis">Diagnosis</label>
                    <input type="text" name="diagnosis" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="nama_dokter">Dokter</label>
                    <input type="text" name="nama_dokter" class="form-control"></input>
                </div>
                 
            </form>
          </div>
          <div class="modal-footer">
           <button type="button" class="btn btn-success" id="btn_add_data">Simpan</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
 
      </div>
    </div>
 
    <!-- Modal Edit-->
    <div id="editModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
 
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Edit Data</h4>
          </div>
          <div class="modal-body">
            <form>
            	<div class="form-group">
                    <label for="id_riwayat">ID</label>
                    <input type="text" name="id_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="usia">Usia</label>
                    <input type="text" name="usia_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" name="alamat_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="tgl">Tanggal Periksa</label>
                    <input type="date" name="tgl_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="keluhan">Keluhan</label>
                    <input type="text" name="keluhan_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="diagnosis">Diagnosis</label>
                    <input type="text" name="diagnosis_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="nama_dokter">Dokter</label>
                    <input type="text" name="dkt_edit" class="form-control"></input>
                </div>
                
 
            </form>
          </div>
          <div class="modal-footer">
           <button type="button" class="btn btn-success" id="btn_update_data">Update</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
 
      </div>
    </div>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        tampil_data();
        //Menampilkan Data di tabel
        function tampil_data(){
            $.ajax({
                url: '<?php echo site_url('Riwayat/ambilData'); ?>',
                type: 'POST',
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    var i;
                    var no = 0;
                    var html = "";
                    for(i=0;i < response.length ; i++){
                        no++;
                        html = html + '<tr>'
                                    + '<td>' + no  + '</td>'
                                    + '<td>' + response[i].id_riwayat + '</td>'
                                    + '<td>' + response[i].nama  + '</td>'
                                    + '<td>' + response[i].usia  + '</td>'
                                    + '<td>' + response[i].alamat  + '</td>'
                                    + '<td>' + response[i].tgl  + '</td>'
                                    + '<td>' + response[i].keluhan  + '</td>'
                                    + '<td>' + response[i].diagnosis  + '</td>'
                                    + '<td>' + response[i].nama_dokter  + '</td>'
                                    + '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].id_riwayat+'" class="btn btn-success btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].id_riwayat+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
                                    + '</tr>';
                    }
                    $("#tbl_data").html(html);
                }
 
            });
        }
        //Hapus Data dengan konfirmasi
        $("#tbl_data").on('click','.btn_hapus',function(){
            var id_riwayat = $(this).attr('data-id');
            var status = confirm('Apakah anda yakin ingin menghapus data ini?');
            if(status){
                $.ajax({
                    url: '<?php echo site_url('Riwayat/hapusData'); ?>',
                    type: 'POST',
                    data: {id_riwayat:id_riwayat},
                    success: function(response){
                        tampil_data();
                    }
                })
            }
        })
        //Menambahkan Data ke database
        $("#btn_add_data").on('click',function(){
            var id_riwayat = $('input[name="id_riwayat"]').val();
            var nama = $('input[name="nama"]').val();
            var usia = $('input[name="usia"]').val();
            var alamat = $('input[name="alamat"]').val();
            var tgl = $('input[name="tgl"]').val();
            var keluhan = $('input[name="keluhan"]').val();
            var diagnosis = $('select[name="diagnosis"]').val();
            var nama_dokter = $('select[name="nama_dokter"]').val();
            $.ajax({
                url: '<?php echo site_url('Riwayat/tambahData'); ?>',
                type: 'POST',
                data: {id_riwayat:id_riwayat,nama:nama,usia:usia,alamat:alamat,tgl:tgl,keluhan:keluhan,diagnosis:diagnosis,nama_dokter:nama_dokter},
                success: function(response){
                    $('input[name="id_riwayat"]').val("");
                    $('input[name="nama"]').val("");
                    $('input[name="usiasia"]').val("");
                    $('input[name="alamat"]').val("");
                    $('input[name="tgl"]').val("");
                    $('input[name="keluhan"]').val("");
                    $('input[name="diagnosis"]').val("");
                    $('select[name="nama_dokter"]').val("");
                    $("#addModal").modal('hide');
                    tampil_data();
                }
            })
 
        });
            //Memunculkan modal edit
        $("#tbl_data").on('click','.btn_edit',function(){
            var id_riwayat = $(this).attr('data-id');
            $.ajax({
                url: '<?php echo site_url('Riwayat/ambilDataByNoinduk'); ?>',
                type: 'POST',
                data: {id_riwayat:id_riwayat},
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    $("#editModal").modal('show');
                    $('input[name="id_edit"]').val(response[0].id_riwayat);
                    $('input[name="nama_edit"]').val(response[0].nama);
                    $('input[name="usia_edit"]').val(response[0].usia);
                    $('input[name="alamat_edit"]').val(response[0].alamat);
                    $('input[name="tgl_edit"]').val(response[0].tgl);
                    $('input[name="keluhan_edit"]').val(response[0].keluhan);
                    $('input[name="diagnosis_edit"]').val(response[0].diagnosis);
                    $('input[name="dkt_edit"]').val(response[0].nama_dokter);
                }
            })
        });

        //Meng-Update Data
        $("#btn_update_data").on('click',function(){
            var id_riwayat = $('input[name="id_edit"]').val();
            var nama = $('input[name="nama_edit"]').val();
            var usia = $('input[name="usia_edit"]').val();
            var alamat = $('input[name="alamat_edit"]').val();
            var tgl = $('input[name="tgl_edit"]').val();
            var keluhan = $('input[name="keluhan_edit"]').val();
            var diagnosis = $('input[name="diagnosis_edit"]').val();
            var nama_dokter = $('input[name="dkt_edit"]').val();
            $.ajax({
                url: '<?php echo site_url('Riwayat/perbaruiData'); ?>',
                type: 'POST',
                data: {id_riwayat:id_riwayat,nama:nama,usia:usia,alamat:alamat,tgl:tgl,keluhan:keluhan,diagnosis:diagnosis,nama_dokter:nama_dokter},
                success: function(response){
                    $('input[name="id_edit"]').val("");
                    $('input[name="nama_edit"]').val("");
                    $('input[name="usia_edit"]').val("");
                    $('input[name="alamat_edit"]').val("");
                    $('input[name="tgl_edit"]').val("");
                    $('input[name="keluhan_edit"]').val("");
                    $('input[name="diagnosis_edit"]').val("");
                    $('input[name="dkt_edit"]').val("");
                    $("#editModal").modal('hide');
                    tampil_data();
                }
            })

        });
    });
</script>