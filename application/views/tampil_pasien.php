<!DOCTYPE html>
<html>
<head>
	<title>Tampil Pasien</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'bootstrap/css/bootstrap.css'?>">
	 <script type="text/javascript" src="<?php echo base_url('asset/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>

</head>
<body>
    <br>
    <div class="container-fluid">
	 <div class="panel panel-primary">
        <div class="panel-heading">
            <div align="right">
            <center><h3><b class="col-md-10">DATA PASIEN</b></h3></center>
                <button data-toggle="modal" data-target="#addModal" class="btn btn-success"><b>+ </b>Tambah Data</button>
                <a href="<?=base_url()?>index.php"><button class="btn btn-link">BACK</button></a>
            </div>
        </div>
        <br>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="bg-primary">
                            <th>No</th>
                            <th>ID</th>
							<th>Nama</th>
							<th>Alamat</th>
							<th>No Telphone</th>
							<th>Usia</th>
							<th>Jenis Kelamin</th>
                            <th>Dokter</th>
							<th>Option</th>
                        </tr>
                    </thead>
                    <tbody id="tbl_data">
                         <!-- isi tabel-->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
 
    <!-- Modal Tambah-->
    <div id="addModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
 
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Tambah Data</h4>
          </div>
          <div class="modal-body">
            <form>
                
                <div class="form-group">
                    <label for="nama_pasien">Nama</label>
                    <input type="text" name="nama_pasien" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="alamat_pasien">Alamat</label>
                    <input type="text" name="alamat_pasien" class="form-control"></input>
                </div>
                <div class="form-group" id="only-number">
                    <label for="nomer_pasien">Nomor Telphone</label>
                    <input type="text" name="nomer_pasien" class="form-control" id="number" placeholder="Input only number">
                </div>
                <div class="form-group">
                    <label for="usia_pasien">Usia</label>
                    <input type="number" name="usia_pasien" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="jk">Jenis Kelamin</label>
                    <select name="jk" id="jk" class="form-control">
                        <option value="">--PILIH--</option>
                        <option value="P">P</option>
                        <option value="L">L</option>
                    </select>
                </div>
                 <div class="form-group">
                    <label for="dkt">Dokter</label>
                        <select name="id_dkt" id="id_dkt" class="form-control">
                            <option value="">PILIH DOKTER</option>
                            <?php foreach($tb_dokter as $row):?>
                                <option value="<?php echo $row->id_dokter;?>"><?php echo $row->nama_dokter;?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
            </form>
          </div>
          <div class="modal-footer">
           <button type="button" class="btn btn-success" id="btn_add_data">Simpan</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
 
      </div>
    </div>
 
    <!-- Modal Edit-->
    <div id="editModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
 
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Edit Data</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="nama_pasien">Nama</label>
                    <input type="text" name="nama_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="alamat_pasien">Alamat</label>
                    <input type="text" name="alamat_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="nomer_pasien">No Telepon</label>
                    <input type="text" name="no_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="usia_pasien">Usia</label>
                    <input type="number" name="usia_edit" class="form-control"></input>
                </div>
                <div class="form-group">
                    <label for="jk">Jenis Kelamin</label>
                    <select name="jk" id="jk" class="form-control">
                        <option value="P"<?php echo "selected"?>>Perempaun</option>
                        <option value="L"<?php echo "selected"?>>Laki-laki</option>
                    </select>
                </div>
                 <div class="form-group">
                    <label for="dkt">Dokter</label>
                        <select name="dkt" id="dkt" class="form-control">
                            <option value="">PILIH DOKTER</option>
                            <?php foreach($tb_dokter as $row):?>
                                <option value="<?php echo $row->id_dokter;?>" <?php echo $tb_dokter->id_dokter=='<?php echo $row->id_dokter;?>'?'selected':'' ?>><?php echo $row->nama_dokter;?>
                                </option>
                            <?php endforeach;?>
                        </select>
                    </div>
            </form>
          </div>
          <div class="modal-footer">
           <button type="button" class="btn btn-success" id="btn_update_data">Update</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
 
      </div>
    </div>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        tampil_data();
        //Menampilkan Data di tabel
        function tampil_data(){
            $.ajax({
                url: '<?php echo site_url('Pasien/ambilData'); ?>',
                type: 'POST',
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    var i;
                    var no = 0;
                    var html = "";
                    for(i=0;i < response.length ; i++){
                        no++;
                        html = html + '<tr>'
                                    + '<td>' + no  + '</td>'
                                    + '<td>' + response[i].id_pasien + '</td>'
                                    + '<td>' + response[i].nama_pasien  + '</td>'
                                    + '<td>' + response[i].alamat_pasien  + '</td>'
                                    + '<td>' + response[i].nomer_pasien  + '</td>'
                                    + '<td>' + response[i].usia_pasien  + '</td>'
                                    + '<td>' + response[i].jk  + '</td>'
                                    + '<td>' + response[i].id_dkt  + '</td>'
                                    + '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].id_pasien+'" class="btn btn-success btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].id_pasien+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
                                    + '</tr>';
                    }
                    $("#tbl_data").html(html);
                }
 
            });
        }

        //Hapus Data dengan konfirmasi
        $("#tbl_data").on('click','.btn_hapus',function(){
            var id_pasien = $(this).attr('data-id');
            var status = confirm('Apakah anda yakin ingin menghapus data ini?');
            if(status){
                $.ajax({
                    url: '<?php echo site_url('Pasien/hapusData'); ?>',
                    type: 'POST',
                    data: {id_pasien:id_pasien},
                    success: function(response){
                        tampil_data();
                    }
                })
            }
        })

        //input only number
            $(function() {
      $('#only-number').on('keydown', '#number', function(e){
          -1!==$
          .inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
          .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
          || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
          && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
      });
    })

        //Menambahkan Data ke database
        $("#btn_add_data").on('click',function(){
            var id_pasien = $('input[name="id_pasien"]').val();
            var nama_pasien = $('input[name="nama_pasien"]').val();
            var alamat_pasien = $('input[name="alamat_pasien"]').val();
            var nomer_pasien = $('input[name="nomer_pasien"]').val();
            var usia_pasien = $('input[name="usia_pasien"]').val();
            var jk = $('select[name="jk"]').val();
            var id_dkt = $('select[name="id_dkt"]').val();
            $.ajax({
                url: '<?php echo site_url('Pasien/tambahData'); ?>',
                type: 'POST',
                data: {id_pasien:id_pasien,nama_pasien:nama_pasien,alamat_pasien:alamat_pasien,nomer_pasien:nomer_pasien,usia_pasien:usia_pasien,jk:jk,id_dkt:id_dkt},
                success: function(response){
                    $('input[name="id_pasien"]').val("");
                    $('input[name="nama_pasien"]').val("");
                    $('input[name="alamat_pasien"]').val("");
                    $('input[name="nomer_pasien"]').val("");
                    $('input[name="usia_pasien"]').val("");
                    $('select[name="jk"]').val("");
                    $('select[name="id_dkt"]').val("");
                    $("#addModal").modal('hide');
                    tampil_data();
                }
            })
 
        });

            //Memunculkan modal edit
        $("#tbl_data").on('click','.btn_edit',function(){
            var id_pasien = $(this).attr('data-id');
            $.ajax({
                url: '<?php echo site_url('Pasien/ambilDataByIdPasien'); ?>',
                type: 'POST',
                data: {id_pasien:id_pasien},
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    $("#editModal").modal('show');
                    $('input[name="id_edit"]').val(response[0].id_pasien);
                    $('input[name="nama_edit"]').val(response[0].nama_pasien);
                    $('input[name="alamat_edit"]').val(response[0].alamat_pasien);
                    $('input[name="no_edit"]').val(response[0].nomer_pasien);
                    $('input[name="usia_edit"]').val(response[0].usia_pasien);
                    $('select[name="jk"]').val(response[0].jk);
                    $('select[name="dokter_siti"]').val(response[0].id_dkt);
                }
            })
        });

        //Meng-Update Data
        $("#btn_update_data").on('click',function(){
            var id_pasien = $('input[name="id_edit"]').val();
            var nama_pasien = $('input[name="nama_edit"]').val();
            var alamat_pasien = $('input[name="alamat_edit"]').val();
            var nomer_pasien = $('input[name="no_edit"]').val();
            var usia_pasien = $('input[name="usia_edit"]').val();
            var jk = $('input[name="jk"]').val();
            var id_dkt = $('input[name="dkt_edit"]').val();
            $.ajax({
                url: '<?php echo site_url('Pasien/perbaruiData'); ?>',
                type: 'POST',
                data: {id_pasien:id_pasien,nama_pasien:nama_pasien,alamat_pasien:alamat_pasien,nomer_pasien:nomer_pasien,usia_pasien:usia_pasien,jk:jk,id_dkt:id_dkt},
                success: function(response){
                    $('input[name="id_edit"]').val("");
                    $('input[name="nama_edit"]').val("");
                    $('input[name="alamat_edit"]').val("");
                    $('input[name="no_edit"]').val("");
                    $('input[name="usia_edit"]').val("");
                    $('select[name="jk"]').val("");
                    $('select[name="dokter_siti"]').val("");
                    $("#editModal").modal('hide');
                    tampil_data();
                }
            })

        });
    });
</script>