<!DOCTYPE html>
<html>
<head>
	<title>Edit Data</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'bootstrap/css/bootstrap.css'?>">
</head>
<body>
	<center>
		<h1></h1>
		<h3>Edit Data</h3>
	</center>
	<div class="jumbotron">
	<form action="<?php echo site_url('dokter/update')?>" method="post">
		<table style="margin:20px auto;">
			<div class="form-group row">
                    <?php foreach($dokter_siti as $u){ ?>
    		<label class="col-sm-2 col-form-label">Nama</label>
    			<div class="col-sm-10">
					<input type="hidden" name="id_dokter" value="<?php echo $u->id_dokter ?>">
					<input type="text" name="nama_dokter" value="<?php echo $u->nama_dokter ?>">
    			</div>
    		</div>
    		<div class="form-group row">
    		<label class="col-sm-2 col-form-label">Alamat</label>
    			<div class="col-sm-10">
					<input type="hidden" name="id_dokter" value="<?php echo $u->id_dokter ?>">
					<input type="text" name="alamat" value="<?php echo $u->alamat ?>">
    			</div>
    		</div>
    		<div class="form-group row">
    		<label class="col-sm-2 col-form-label">No Telphone</label>
    			<div class="col-sm-10">
					<input type="hidden" name="id_dokter" value="<?php echo $u->id_dokter ?>">
					<input type="text" name="notlp" value="<?php echo $u->notlp?>">
    			</div>
    		</div>
    		<div class="form-group row">
    		<label class="col-sm-2 col-form-label">Email</label>
    			<div class="col-sm-10">
					<input type="hidden" name="id_dokter" value="<?php echo $u->id_dokter ?>">
					<input type="email" name="email" value="<?php echo $u->email ?>">
    			</div>
    		</div>
    		<div class="form-group row">
    		<label class="col-sm-2 col-form-label">Spesialis</label>
    			<div class="col-sm-10">
					<input type="hidden" name="id_dokter" value="<?php echo $u->id_dokter ?>">
					<input type="text" name="spesialis" value="<?php echo $u->spesialis ?>">
    			</div>
    		</div>
    		<div class="form-group row">
    		<label class="col-sm-2 col-form-label">Jadwal</label>
    			<div class="col-sm-10">
					<input type="hidden" name="id_dokter" value="<?php echo $u->id_dokter ?>">
					<input type="text" name="jadwal" value="<?php echo $u->jadwal ?>">
    			</div>
    		</div>
			
			<button type="submit" class="btn btn-primary">Submit</button>
		</table>
	</form>	
	<?php } ?>
</div>
</body>
</html>