<!DOCTYPE html>
<html>
<head>
    <title>Tampil Dokter</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'bootstrap/css/bootstrap.css'?>">
     <script type="text/javascript" src="<?php echo base_url('asset/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>

</head>
<body>
    <br>
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div align="right">
                    <center><h3><b class="col-md-10">DATA DOKTER</b></h3></center>
                    <button data-toggle="modal" data-target="#addModal" class="btn btn-success"><b>+ </b>Tambah Data</button>
                    <a href="<?=base_url()?>index.php"><button class="btn btn-link">BACK</button></a>
                </div>
            </div>
        <br>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="bg-warning">
                            <th>No</th>
                            <th>ID</th>
                            <th>Nama Dokter</th>
                            <th>Alamat</th>
                            <th>No Telphone</th>
                            <th>Email</th>
                            <th>Spesialis</th>
                            <th>Jadwal</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody id="tbl_data">
                         <!-- isi tabel-->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
 
    <!-- Modal Tambah-->
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="nama_dokter">Nama Dokter</label>
                                <input type="text" name="nama_dokter" class="form-control"></input>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <input type="text" name="alamat" class="form-control"></input>
                            </div>
                            <div class="form-group" id="only-number">
                                <label for="notlp">Nomor Telphone</label>
                                <input type="text" name="notlp" class="form-control" id="number">
                                <label>*input hanya angka</label>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control"></input>
                            </div>
                            <div class="form-group">
                                <label for="spesialis">Spesialis</label>
                                <input type="text" name="spesialis" class="form-control"></input>
                            </div>
                             <div class="form-group">
                                <label for="jadwal">Jadwal</label>
                                    <select name="jadwal" id="jadwal" class="form-control">
                                        <option value="">PILIH JADWAL</option>
                                        <?php foreach($tb_jadwal as $row):?>
                                            <option value="<?php echo $row->id_jadwal;?>"><?php echo $row->sub_jadwal;?></option>
                                        <?php endforeach;?>
                                    </select>
                            </div>
                        </form>
                    </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btn_add_data">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
 
        </div>
    </div>
 
    <!-- Modal Edit-->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Edit Data</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="nama_dokter">Nama</label>
                            <input type="text" name="nama_edit" class="form-control"></input>
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" name="alamat_edit" class="form-control"></input>
                        </div>
                        <div class="form-group">
                            <label for="notlp">No Telepon</label>
                            <input type="text" name="no_edit" class="form-control"></input>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email_edit" class="form-control"></input>
                        </div>
                        <div class="form-group">
                            <label for="spesialis">Spesialis</label>
                            <input type="text" name="spesialis_edit" class="form-control"></input>
                        </div>
                         <div class="form-group">
                            <label>Jadwal</label>
                            <select name="jadwal_edit" class="form-control" required>
                                <?php foreach($tb_jadwal as $row) { ?>
                                    <option value="<?=$row->id_jadwal?>"><?=$row->sub_jadwal?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btn_update_data">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</html>

<script type="text/javascript">
    $(document).ready(function(){
        tampil_data();
        //Menampilkan Data di tabel
        function tampil_data(){
            $.ajax({
                url: '<?php echo site_url('Dokter/ambilData'); ?>',
                type: 'POST',
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    var i;
                    var no = 0;
                    var html = "";
                    for(i=0;i < response.length ; i++){
                        no++;
                        html = html + '<tr>'
                                    + '<td>' + no  + '</td>'
                                    + '<td>' + response[i].id_dokter + '</td>'
                                    + '<td>' + response[i].nama_dokter  + '</td>'
                                    + '<td>' + response[i].alamat  + '</td>'
                                    + '<td>' + response[i].notlp  + '</td>'
                                    + '<td>' + response[i].email  + '</td>'
                                    + '<td>' + response[i].spesialis  + '</td>'
                                    + '<td>' + response[i].jadwal  + '</td>'
                                    + '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].id_dokter+'" class="btn btn-success btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].id_dokter+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
                                    + '</tr>';
                    }
                    $("#tbl_data").html(html);
                }
            });
        }

        //input nomor telphone yang only number
            $(function() {
              $('#only-number').on('keydown', '#number', function(e){
                  -1!==$
                  .inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
                  .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
                  || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
                  && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
              });
            })

        //Hapus Data dengan konfirmasi
        $("#tbl_data").on('click','.btn_hapus',function(){
            var id_dokter = $(this).attr('data-id');
            var status = confirm('Apakah anda yakin ingin menghapus data ini?');
            if(status){
                $.ajax({
                    url: '<?php echo site_url('Dokter/hapusData'); ?>',
                    type: 'POST',
                    data: {id_dokter:id_dokter},
                    success: function(response){
                        tampil_data();
                    }
                })
            }
        })

        //Menambahkan Data ke database
        $("#btn_add_data").on('click',function(){
            var id_dokter = $('input[name="id_dokter"]').val();
            var nama_dokter = $('input[name="nama_dokter"]').val();
            var alamat = $('input[name="alamat"]').val();
            var notlp = $('input[name="notlp"]').val();
            var email = $('input[name="email"]').val();
            var spesialis = $('input[name="spesialis"]').val();
            var jadwal = $('select[name="jadwal"]').val();
            $.ajax({
                url: '<?php echo site_url('Dokter/tambahData'); ?>',
                type: 'POST',
                data: {id_dokter:id_dokter,nama_dokter:nama_dokter,alamat:alamat,notlp:notlp,email:email,spesialis:spesialis,jadwal:jadwal},
                success: function(response){
                    $('input[name="id_dokter"]').val("");
                    $('input[name="nama_dokter"]').val("");
                    $('input[name="alamat"]').val("");
                    $('input[name="notlp"]').val("");
                    $('input[name="email"]').val("");
                    $('input[name="spesialis"]').val("");
                    $('select[name="jadwal"]').val("");
                    $("#addModal").modal('hide');
                    tampil_data();
                }
            })
 
        });

            //Memunculkan modal edit
        $("#tbl_data").on('click','.btn_edit',function(){
            var id_dokter = $(this).attr('data-id');
            $.ajax({
                url: '<?php echo site_url('Dokter/ambilDataByIdDokter'); ?>',
                type: 'POST',
                data: {id_dokter:id_dokter},
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    $("#editModal").modal('show');
                    $('input[name="id_edit"]').val(response[0].id_dokter);
                    $('input[name="nama_edit"]').val(response[0].nama_dokter);
                    $('input[name="alamat_edit"]').val(response[0].alamat);
                    $('input[name="no_edit"]').val(response[0].notlp);
                    $('input[name="email_edit"]').val(response[0].email);
                    $('input[name="spesialis_edit"]').val(response[0].spesialis);
                    $('input[name="jadwal_edit"]').val(response[0].jadwal);
                }
            })
        });

        //Meng-Update Data
        $("#btn_update_data").on('click',function(){
            var id_dokter = $('input[name="id_edit"]').val();
            var nama_dokter = $('input[name="nama_edit"]').val();
            var alamat = $('input[name="alamat_edit"]').val();
            var notlp = $('input[name="no_edit"]').val();
            var email = $('input[name="email_edit"]').val();
            var spesialis = $('input[name="spesialis_edit"]').val();
            var jadwal = $('input[name="jadwal_edit"]').val();
            $.ajax({
                url: '<?php echo site_url('Dokter/perbaruiData'); ?>',
                type: 'POST',
                data: {id_dokter:id_dokter,nama_dokter:nama_dokter,alamat:alamat,notlp:notlp,email:email,spesialis:spesialis,jadwal:jadwal},
                success: function(response){
                    $('input[name="id_edit"]').val("");
                    $('input[name="nama_edit"]').val("");
                    $('input[name="alamat_edit"]').val("");
                    $('input[name="no_edit"]').val("");
                    $('input[name="email_edit"]').val("");
                    $('input[name="spesialis_edit"]').val("");
                    $('input[name="jadwal_edit"]').val("");
                    $("#editModal").modal('hide');
                    tampil_data();
                }
            })

        });
    });
</script> 