<?php

Class Riwayat extends CI_Controller {

		function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('url'));
		$this->load->model('riwayat_model');
		}
 
      public function index()
        { 
            $data['tb_riwayat']=$this->riwayat_model->getAll();
            $this->load->view('tampil_riwayat',$data);
        }
     
     function ambilData(){
            $data = $this->riwayat_model->tampil_tabel();
            echo json_encode($data);
        }
     
        function ambilDataByNoinduk(){
            $id_riwayat = $this->input->post('id_riwayat');
            $data = $this->riwayat_model->getDataByNoinduk($id_riwayat);
            echo json_encode($data);
        }
     
        function hapusData(){
            $id_riwayat = $this->input->post('id_riwayat');
            $data = $this->riwayat_model->deleteData($id_riwayat);
            echo json_encode($data);
        }
     
        function tambahData(){
            $id_riwayat = $this->input->post('id_riwayat');
    		$nama = $this->input->post('nama');
    		$usia = $this->input->post('usia');
    		$alamat = $this->input->post('alamat');
    		$tanggal = $this->input->post('tanggal');
    		$keluhan = $this->input->post('keluhan');
            $diagnosis = $this->input->post('diagnosis');
            $nama_dokter = $this->input->post('nama_dokter');
     
            $data = ['id_riwayat' => $id_riwayat, 'nama' => $nama, 'usia' => $usia, 'alamat' => $alamat, 'tanggal'=> $tanggal, 'keluhan' => $keluhan, 'diagnosis'=>$diagnosis, 'nama_dokter'=>$nama_dokter];
            $data = $this->riwayat_model->insertData($data);
            echo json_encode($data);
        }
     
        function perbaruiData(){

            $id_riwayat = $this->input->post('id_riwayat');
            $nama = $this->input->post('nama');
            $usia = $this->input->post('usia');
            $alamat = $this->input->post('alamat');
            $tanggal = $this->input->post('tanggal');
            $keluhan = $this->input->post('keluhan');
            $diagnosis = $this->input->post('diagnosis');
            $nama_dokter = $this->input->post('nama_dokter');
     
     
            $data = ['id_riwayat' => $id_riwayat, 'nama' => $nama, 'usia' => $usia, 'alamat' => $alamat, 'tanggal'=> $tanggal, 'keluhan' => $keluhan, 'diagnosis'=>$diagnosis, 'nama_dokter'=>$nama_dokter];
            $data = $this->riwayat_model->updateData($id_riwayat,$data);
            
             
            echo json_encode($data);
        }
        
}

 